class Product < ApplicationRecord
  validates :name, presence: true, length: { maximum: 100 }
  validates :type_product, presence: true
  validates :quantity, presence: true
  validates :price, presence: true
  validates :image, presence: true
  mount_uploader :image, ImageUploader
  enum type_product: { food: "Food", drinks: "Drinks" }
  enum type_food: { appetizer: "Appetizer", dishes: "Main Dishes", desserts: "Desserts"}
  has_many :line_items
  before_destroy :check_if_has_line_item

  private
    def check_if_has_line_item
      if line_items.empty?
          return true
      else
        errors.add(:base, 'This product has a LineItem')
        return false
      end
    end
end
