class StaticPagesController < ApplicationController
  def home
    if params[:phrase].present?
      @q = (Product.where name: params[:phrase]).ransack(params[:q])
      @products_foods = @q.result
      @q = (Product.where type_product: "drinks").ransack(params[:q])
      @products_drinks = @q.result
    else
      @q = (Product.where type_product: "food").ransack(params[:q])
      @products_foods = @q.result
      @q = (Product.where type_product: "drinks").ransack(params[:q])
      @products_drinks = @q.result
    end
    unless params[:q].nil?
      if Search.all.select {|p| p.phrase == params.fetch(:q).fetch(:name_cont)}.present?
        search_cont = Search.all.select {|p| p.phrase == params.fetch(:q).fetch(:name_cont)}
        count_keyword = search_cont[0].count_keyword
        search_cont[0].update(count_keyword: count_keyword + 1 )
      else
        Search.create!(
          phrase: params.fetch(:q).fetch(:name_cont)
        )
      end
    end
    @searchs = Search.paginate(:page => params[:page], :per_page => 5).order(count_keyword: :DESC)
  end

  def appetizer
    @q = (Product.where type_food: "appetizer").ransack(params[:q])
    @appetizers = @q.result
    @q = (Product.where type_product: "drinks").ransack(params[:q])
    @products_drinks = @q.result
  end

  def dishes
    @q = (Product.where type_food: "dishes").ransack(params[:q])
    @dishes = @q.result
    @q = (Product.where type_product: "drinks").ransack(params[:q])
    @products_drinks = @q.result
  end

  def desserts
    @q = (Product.where type_food: "desserts").ransack(params[:q])
    @desserts = @q.result
    @q = (Product.where type_product: "drinks").ransack(params[:q])
    @products_drinks = @q.result
  end
end
