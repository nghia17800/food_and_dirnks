class ProductsController < ApplicationController
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "Create product success!"
      redirect_to products_path
    else
      flash[:danger] = "The data entry is missing!"
      render :new
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :type_product, :quantity, :price, :description, :image)
  end
end
