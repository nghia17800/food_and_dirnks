# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
10.times do |n|
  name = Faker::Food.sushi
  image = Faker::Avatar
  type_product = "Food"
  quantity = 50
  price = 50000
  description = "abc"
  Product.create!(name: name,
                type_product: type_product,
                quantity: quantity,
                price: price,
                description: description,
                image: image)
end
