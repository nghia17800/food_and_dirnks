class CreateSearches < ActiveRecord::Migration[5.2]
  def change
    create_table :searches do |t|
      t.string :phrase
      t.integer :count_keyword, default: 1

      t.timestamps
    end
  end
end
