Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :orders
  resources :line_items
  resources :carts
  devise_for :users
  get "static_pages/home"
  get "static_pages/appetizer"
  get "static_pages/dishes"
  get "static_pages/desserts"
  root "static_pages#home"
  resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
